object P020 {
    def main(args : Array[String]) {
        val x = BigInt(1) to 100 reduce {_ * _}
        println(x.toString.map{_.asDigit}.sum)
    }
}
