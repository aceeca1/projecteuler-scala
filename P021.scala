object P021 {
    def main(args : Array[String]) {
        val amicable = for {
            i <- 2 until 10000
            di = Divisor.sum(i)
            if (di != i && Divisor.sum(di) == i)
        } yield i
        println(amicable.sum)
    }
}
