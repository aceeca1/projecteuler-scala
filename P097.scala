object P097 {
    def main(args : Array[String]) {
        val m = BigInt("10000000000")
        println((BigInt(2).modPow(7830457, m) * 28433 + 1) % m)
    }
}
