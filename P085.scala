object P085 {
    val m = 2000000
    def segments(n : Int) = n * (n + 1) / 2
    case class Square(a : Int, b : Int) {
        val x = segments(a) * segments(b)
        def diff = (x - m).abs
    }

    def main(args : Array[String]) {
        val Square(a, b) = (Iterator iterate Square(1, 2000)) {
            case s @ Square(a, b) if s.x > m => Square(a, b - 1)
            case Square(a, b) => Square(a + 1, b)
        } takeWhile {_.b > 0} minBy {_.diff}
        println(a * b)
    }
}
