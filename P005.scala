object P005 {
    def main(args : Array[String]) {
        val x = BigInt(1) to 20 reduce {(x1, x2) => x1 / (x1 gcd x2) * x2}
        println(x)
    }
}
