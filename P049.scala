object P049 {
    def main(args : Array[String]) {
        val p = Prime.all.dropWhile{_ < 1000}.takeWhile{_ < 10000}.toVector
        for {
            vi <- p.groupBy{_.toString.sorted}.valuesIterator
            xi <- vi combinations 3
            if (xi(0) + xi(2) == xi(1) + xi(1))
            if (xi.head != 1487)
        } println(xi.mkString)
    }
}
