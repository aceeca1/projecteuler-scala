object P054 {
    val cards = "23456789TJQKA"
    val alpha = "ABCDEFGHIJKLM"
    val cardRank = (cards zip alpha).toMap
    val isStraight = alpha.sliding(5).toSet
    def isFlush(x : Iterable[Char]) = x forall {_ == x.head}

    def hand(s : Array[String]) : String = {
        val F = isFlush(s.map{_(1)})
        val R = s.map{si => cardRank(si(0))}.mkString.sorted
        val S = isStraight(R)
        val S0 = R == "ABCDM"
        val T = R.groupBy(identity).toIterator.map{
            case (k, v) => (v.size, k)
        }.toArray.sorted
        val K = T.map{_._1} match {
            case _ if F && S => "9"
            case _ if F && S0 => "9D"
            case Array(1, 4) => "8"
            case Array(2, 3) => "7"
            case _ if F => "6"
            case _ if S => "5"
            case _ if S0 => "5D"
            case Array(1, 1, 3) => "4"
            case Array(1, 2, 2) => "3"
            case Array(1, 1, 1, 2) => "2"
            case _ => "1"
        }
        K + T.reverseMap{_._2}.mkString
    }

    def win(s : String) = {
        val (x1, x2) = s split " " splitAt 5
        hand(x1) > hand(x2)
    }

    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/poker.txt")
        println(r.getLines count win)
    }
}
