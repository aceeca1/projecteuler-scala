object P117 {
    def main(args : Array[String]) {
        val answer = Iterator.iterate((1L, 2L, 4L, 8L)) {
            case (t1, t2, t3, t4) => (t2, t3, t4, t1 + t2 + t3 + t4)
        }.drop(50 - 1).next._1
        println(answer)
    }
}
