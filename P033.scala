object P033 {
    def main(args : Array[String]) {
        val s = for {
            i1 <- 10 to 99
            i2 <- 10 to 99
            if (i1 != i2)
            (a1, b1) = (i1 / 10, i1 % 10)
            (a2, b2) = (i2 / 10, i2 % 10)
            if (a1 == b2 && i1 * a2 == i2 * b1)
        } yield (i1, i2)
        val a = s.map{x => BigInt(x._1)}.product
        val b = s.map{x => BigInt(x._2)}.product
        println(a / (a gcd b))
    }
}
