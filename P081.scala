object P081 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/matrix.txt").getLines
        val s = r.map{_ split "," map {_.toInt}}.buffered
        val a = Array.fill(s.head.size)(Int.MaxValue)
        a(0) = 0
        for (si <- s) {
            a(0) += si(0)
            def traverse(n : Int) {
                a(n) = si(n) + (a(n - 1) min a(n))
                if (n + 1 < si.size) traverse(n + 1)
            }
            traverse(1)
        }
        println(a.last)
    }
}
