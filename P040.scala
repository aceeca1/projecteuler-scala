object P040 {
    def main(args : Array[String]) {
        val s = Iterator from 1 flatMap {_.toString.toIterator} map {_.asDigit}
        val n = Array(0, 1, 10, 100, 1000, 10000, 100000, 1000000)
        val d = n sliding 2 map {case Array(x1 ,x2) => x2 - x1 - 1}
        println(d.map{s.drop(_).next}.product)
    }
}
