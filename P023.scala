object P023 {
    def main(args : Array[String]) {
        val n = 28123
        val abundant = (2 to n).filter{
            x => Divisor.sum(x) > x
        }.toSet
        val answer = (1 to n).filter{
            x => abundant.forall{ai => !abundant(x - ai)}
        }.sum
        println(answer)
    }
}
