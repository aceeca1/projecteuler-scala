object P076 {
    def p(n : Int) = {
        val r = new Array[Int](n)
        r(0) = 1
        for (p <- 1 until n) {
            def traverse(q : Int) {
                r(q + p) += r(q)
                if (q + 1 + p < n) traverse(q + 1)
            }
            traverse(0)
        }
        r
    }

    def main(args : Array[String]) {
        println(p(101)(100) - 1)
    }
}
