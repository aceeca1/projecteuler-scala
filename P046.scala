object P046 {
    def main(args : Array[String]) {
        val s = Prime.all map {
            x1 => Iterator from 0 map {x2 => x1 + 2 * x2 * x2}
        }
        val sFlat = Infinite flatten s
        val odd = Iterator.iterate(3){_ + 2}
        val oddButNotSFlat = Infinite.diff(odd, sFlat)
        println(oddButNotSFlat.next)
    }
}
