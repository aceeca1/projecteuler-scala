object P042 {
    def isTriangle(x : Int) = {
        val s = x + x
        val n = math.sqrt(s).toInt
        s == n * (n + 1)
    }

    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/words.txt").mkString
        val s = r split "," map {x => x.substring(1, x.size - 1)}
        println(s count {si => isTriangle(si.map{x => x - 'A' + 1}.sum)})
    }
}
