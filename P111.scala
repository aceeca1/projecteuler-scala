object P111 {
    def cartesian(x : Array[Array[Int]]) = (Array(Array[Int]()) /: x) {
        (s, xi) => for {
            i1 <- s
            i2 <- xi
        } yield i1 :+ i2
    }

    def main(args : Array[String]) {
        val n = 10
        val answer = for (d <- 0 to 9) yield Iterator.from(1).map{
            m => {
                val a = Array.fill(m)((0 to 9).filter{_ != d}.toArray)
                val r = for {
                    v <- cartesian(a)
                    p <- 0 until n combinations m
                    x = {
                        val x = Array.fill(n)(d)
                        val vi = v.toIterator
                        for (i <- p) x(i) = vi.next
                        x
                    }
                    if (x.head != 0)
                    xv = (BigInt(0) /: x) {_ * 10 + _}
                    if (xv isProbablePrime 30)
                } yield xv
                r.sum
            }
        }.find{_ > 0}.get
        println(answer.sum)
    }
}
