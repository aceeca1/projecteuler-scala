object P102 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/triangles.txt").getLines
        val s = r map {_ split "," map {_.toInt}}
        val answer = for {
            Array(x1, y1, x2, y2, x3, y3) <- s
            z1 = x2 * y3 - y2 * x3
            z2 = x3 * y1 - y3 * x1
            z3 = x1 * y2 - y1 * x2
            z = Array(z1, z2, z3).map{_.signum}.sum.abs
            if (z == 3)
        } yield ()
        println(answer.size)
    }
}
