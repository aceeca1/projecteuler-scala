object P059 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/cipher1.txt").mkString
        val s = "[0-9]+".r.findAllMatchIn(r).map{_.matched.toInt}
        val c = Array.fill(3){new collection.mutable.ArrayBuffer[Int]}
        val rrC = Infinite.cyclic(c : _*)
        for (i <- s) rrC.next += i
        val p = for (ci <- c) yield {
            val cSpace = ci.groupBy(identity).maxBy{_._2.size}._1
            val key = cSpace ^ ' '
            ci.map{x => (x ^ key).toChar}.mkString.toIterator
        }
        val rrP = Infinite.cyclic(p : _*) take c.map{_.size}.sum
        val pS = for (i <- rrP) yield i.next
        println(pS.map{_.toInt}.sum)
    }
}
