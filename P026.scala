object P026 {
    def main(args : Array[String]) {
        val answer = 1 to 1000 maxBy {
            x => CycleFinding(1){_ * 10 % x}
        }
        println(answer)
    }
}
