object P096 {
    import collection.mutable.BitSet

    val e = Array.tabulate(3, 3, 3, 3){
        (p1, p2, p3, p4) => {
            val q = for {
                q1 <- 0 until 3
                q2 <- 0 until 3
                q3 <- 0 until 3
                q4 <- 0 until 3
                if (
                    p1 == q1 && p2 == q2 ||
                    p3 == q3 && p4 == q4 ||
                    p1 == q1 && p3 == q3
                )
                if (p1 != q1 || p2 != q2 || p3 != q3 || p4 != q4)
            } yield q1 * 27 + q2 * 9 + q3 * 3 + q4
            BitSet(q : _*)
        }
    }.flatten.flatten.flatten

    def let(x : Array[BitSet], m : Int, d : Int) {
        x(m) = BitSet(d)
        for (i <- e(m)) {
            val xs = x(i).size
            x(i) -= d
            if (xs > 1 && x(i).size == 1) let(x, i, x(i).head)
        }
    }

    def search(x : Array[BitSet]) : Array[Int] = {
        val m = x.indices minBy {
            n => x(n).size match {
                case 1 => Int.MaxValue
                case x => x
            }
        }
        val n = x(m).size
        if (n == 0) return null
        if (n == 1) return x map {_.head}
        for (d <- x(m)) {
            val xNext = x map {_.clone}
            let(xNext, m, d)
            val s = search(xNext)
            if (s != null) return s
        }
        return null
    }

    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/sudoku.txt").getLines
        val u = BitSet(1, 2, 3, 4, 5, 6, 7, 8, 9)
        val answer = for (i <- r grouped 10) yield {
            val s = i.tail.flatten.map{_.asDigit}.toArray
            val s0 = Array.fill(81){u.clone}
            for {
                i <- s.indices
                if (s(i) > 0)
            } let(s0, i, s(i))
            val v = search(s0)
            v(0) * 100 + v(1) * 10 + v(2)
        }
        println(answer.sum)
    }
}
