object P047 {
    def hasFour(x : Int) = {
        Prime.factorize(x).distinct.size == 4
    }

    def main(args : Array[String]) {
        val x = Iterator from 1 map hasFour sliding 4 indexWhere {
            _ forall identity
        }
        println(x + 1)
    }
}
