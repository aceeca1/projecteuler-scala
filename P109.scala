object P109 {
    def main(args : Array[String]) {
        val numbers = (1 to 20) :+ 25
        val answer = for {
            num1 <- numbers.toIterator
            ring1 = 2
            num2 <- numbers
            ring2 <- 0 to 3
            if (ring2 != 0 || num2 == 1)
            if (ring2 != 3 || num2 != 25)
            num3 <- numbers
            ring3 <- 0 to 3
            if (ring3 != 0 || num3 == 1)
            if (ring3 != 3 || num3 != 25)
            if (ring3 > ring2 || ring3 == ring2 && num3 >= num2)
            checkout = ring1 * num1 + ring2 * num2 + ring3 * num3
            if (checkout < 100)
        } yield checkout;
        println(answer.size);
    }
}
