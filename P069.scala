object P069 {
    def main(args : Array[String]) {
        val answer = 2 to 1000000 minBy {
            Prime.factorize(_).distinct.map{1.0 - 1.0 / _}.product
        }
        println(answer)
    }
}
