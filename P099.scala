object P099 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/base_exp.txt").getLines
        val s = r.map{_ split "," map {_.toDouble}}.zipWithIndex
        val answer = s maxBy {
            case (Array(base, exp), _) => (math log base) * exp
        }
        println(answer._2 + 1)
    }
}
