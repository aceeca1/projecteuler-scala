object P013 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/problem=13").mkString
        val s = "[0-9]+(?=<br)".r.findAllMatchIn(r).map{x => BigInt(x.matched)}.sum.toString
        println(s take 10)
    }
}
