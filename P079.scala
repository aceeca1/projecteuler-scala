object P079 {
    case class Node(s : String, v : Vector[String]) {
        val succeed = v.isEmpty
        def h = if (succeed) 0 else v.map{_.size}.max
        val prio = -(s.size + h)
        def childs = {
            for {
                i <- v.map{_.head}.distinct
                vx = for {
                    vi <- v
                    vix = if (vi.head == i) vi.tail else vi
                    if (vix.size != 0)
                } yield vix
            } yield Node(s + i, vx)
        }
    }

    val q = collection.mutable.PriorityQueue[Node]()(Ordering.by{_.prio})

    def search() : Node = {
        val x = q.dequeue
        if (x.succeed) x else {
            q.enqueue(x.childs : _*)
            search()
        }
    }

    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/keylog.txt").getLines
        q.enqueue(Node("", r.toVector))
        println(search().s)
    }
}
