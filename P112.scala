object P112 {
    def main(args : Array[String]) {
        val answer = Iterator.from(1).scanLeft((0, 0)) {
            case ((total, bouncy), x) => {
                val s = x.toString
                val ss = s.sorted
                val b = if (s == ss || s == ss.reverse) bouncy else bouncy + 1
                (total + 1, b)
            }
        } drop 1 indexWhere {
            case (total, bouncy) => total * 99 == bouncy * 100
        }
        println(answer + 1)
    }
}
