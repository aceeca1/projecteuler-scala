object P088 {
    val n = 12000
    val mp = Array.fill(n + 1)(Int.MaxValue)

    def DFS(pSum : Int, pProduct : Int, cSize : Int, last : Int) {
        val cSum = pSum + last
        val cProduct = pProduct * last
        val finalSize = cProduct - cSum + cSize
        if (finalSize > n) return
        if (cSize == 1 && (last - 1) * (last - 1) > n) return
        mp(finalSize) = mp(finalSize) min cProduct
        DFS(pSum, pProduct, cSize, last + 1)
        DFS(cSum, cProduct, cSize + 1, last)
    }

    def main(args : Array[String]) {
        DFS(0, 1, 1, 2)
        println(mp.drop(2).distinct.sum)
    }
}
