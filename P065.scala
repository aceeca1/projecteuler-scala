object P065 {
    def main(args : Array[String]) {
        val s1 = Iterator.iterate(2){_ + 2}
        val s2 = s1 flatMap {x => Vector(1, x, 1)}
        val s3 = Iterator(2) ++ s2
        val answer = (s3 take 100 foldRight (BigInt(1), BigInt(0))) {
            (x, s) => (s._2 + s._1 * x, s._1)
        }
        println(answer._1.toString.map{_.asDigit}.sum)
    }
}
