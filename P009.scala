object P009 {
    def main(args : Array[String]) {
        val abc = for {
            m <- 1 to 500
            if (500 % m == 0)
            kp = 500 / m
            p <- m + 1 until m * 2
            if (kp % p == 0)
            k = kp / p
            n = p - m
            m2 = m * m
            n2 = n * n
        } yield Array(k * (m2 - n2), 2 * k * m * n, k * (m2 + n2))
        println(abc.head.product)
    }
}
