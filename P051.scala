object P051 {
    def main(args : Array[String]) {
        val answer = for {
            p <- Prime.all.toIterator
            s = p.toString
            C1 <- "012"
            if (s contains C1)
            si <- (Array("") /: s) {
                (p, c) => c match {
                    case C1 => p flatMap {pi => Array(pi + C1, pi + '*')}
                    case _ => p map {_ + c}
                }
            }.toIterator.drop(1)
            num = (C1 + 1).toChar to '9' count {
                C2 => Prime(si.replace('*', C2).toInt)
            }
            if (num >= 7)
        } yield p
        println(answer.next)
    }
}
