object P077 {
    def p(n : Int) = {
        val r = new Array[Int](n)
        r(0) = 1
        for (p <- Prime.all takeWhile {_ < n}) {
            def traverse(q : Int) {
                r(q + p) += r(q)
                if (q + 1 + p < n) traverse(q + 1)
            }
            traverse(0)
        }
        r
    }

    def main(args : Array[String]) {
        val answer = (Iterator iterate (0, p(1))) {
            case (x, a) if x + 1 >= a.size => (x + 1, p(a.size.*(1.5).toInt + 1))
            case (x, a) => (x + 1, a)
        } find {
            case (x, a) => a(x) > 5000
        }
        println(answer.get._1)
    }
}
