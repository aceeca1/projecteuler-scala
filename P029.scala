object P029 {
    def main(args : Array[String]) {
        val s = Iterator.iterate(Array.fill(99)(BigInt(1))){
            x => (x, 2 to 100).zipped map {_ * _}
        } drop 2 take 99
        println(s.flatten.toBuffer.distinct.size)
    }
}
