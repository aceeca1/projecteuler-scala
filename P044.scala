object P044 {
    def main(args : Array[String]) {
        val answer = for {
            a <- Iterator from 1
            aP = PolygonalNum(5)(a)
            pq = aP * 2
            pB <- Divisor(pq)
            p = pB.toLong
            q = pq / p
            b = (p - 3 * q + 1) / 6
            if (b > 0)
            c = b + q
            if (pq == (3 * c + 3 * b - 1) * (c - b))
            bP = PolygonalNum(5)(b)
            cP = aP + bP
            dP = bP + cP
            if (PolygonalNum.test(5)(dP))
        } yield aP
        println(answer.next)
    }
}
