object P114 {
    def main(args : Array[String]) {
        val answer = Iterator.iterate((0L, 1L, 1L, 2L)) {
            case (s, t0, t1, t2) => (s + t0, t1, t2, t2 + s + 2L)
        }.drop(50 - 1).next._2
        println(answer)
    }
}
