object P070 {
    val n = 10000000
    val phi = Prime.phi(n)

    def sameDigitWithPhi(x : Int) = {
        val p = phi(x)
        (p - x) % 9 == 0 && p.toString.sorted == x.toString.sorted
    }

    def main(args : Array[String]) {
        val answer = 2 until n filter sameDigitWithPhi minBy {
            x => x.toDouble / phi(x)
        }
        println(answer)
    }
}
