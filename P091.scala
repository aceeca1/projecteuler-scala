object P091 {
    def main(args : Array[String]) {
        val n = 50
        val answer = for {
            i1 <- 1 to n
            i2 <- 1 to n
            d = BigInt(i1).gcd(i2).toInt
        } yield {
            def f(x1 : Int, x2 : Int) = ((n - x1) min (x2 * x2 / x1)) / (x2 / d)
            f(i1, i2) + f(i2, i1)
        }
        println(answer.sum + 3 * n * n)
    }
}
