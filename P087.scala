object P087 {
    val n = 50000000

    def main(args : Array[String]) {
        val p2 = Prime.all.map{x => x * x}.takeWhile{_ < n}.toBuffer
        val p3 = Prime.all.map{x => x * x * x}.takeWhile{_ < n}.toBuffer
        val p4 = Prime.all.map{x => x * x * x * x}.takeWhile{_ < n}.toBuffer
        val answer = for {
            i2 <- p2
            i3 <- p3
            i4 <- p4
            x = i2 + i3 + i4
            if (x < n)
        } yield x
        println(answer.distinct.size)
    }
}
