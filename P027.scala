object P027 {
    def main(args : Array[String]) {
        val ab = for {
            b <- Prime.all takeWhile {_ < 1000}
            a <- -1000 to 1000
            if (Prime(b + a + 1))
            if (Prime(b + a * 2 + 4))
        } yield Array(a, b)
        val answer = ab maxBy {
            case Array(a, b) => Iterator from 0 indexWhere {
                n => !Prime(b + a * n + n * n)
            }
        }
        println(answer.product)
    }
}
