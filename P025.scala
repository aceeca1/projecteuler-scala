object P025 {
    def fibFrom(x1 : BigInt, x2 : BigInt) : Stream[BigInt] = x1 #:: fibFrom(x2, x1 + x2)
    val fib = fibFrom(1, 1)
    def main(args : Array[String]) {
        val x = fib indexWhere {_.toString.size >= 1000}
        println(x + 1)
    }
}
