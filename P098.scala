object P098 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/words.txt").mkString
        val s = r split "," map {x => x.substring(1, x.size - 1)}
        val sgv = s.groupBy{_.sorted}.values
        val sgvc = sgv filter {_.size > 1} flatMap {_ combinations 2}
        val answer = for {
            Array(s1, s2) <- sgvc
            n = s1.size
            lowerBound = math sqrt ("1" + "0" * (n - 1)).toInt
            upperBound = math sqrt ("9" * n).toInt
            i1 <- lowerBound.ceil.toInt to upperBound.toInt
            x1 = (i1 * i1).toString
            if (s1 == x1.map{(x1, s1).zipped.toMap})
            sx = (s1, x1).zipped.toMap
            if (x1 == s1.map{sx})
            x2 = s2 map sx
            i2 = (math sqrt x2.toInt).toInt
            if (x2 == (i2 * i2).toString)
            x <- Array(x1, x2)
        } yield x.toInt
        println(answer.max)
    }
}
