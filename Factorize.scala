object Factorize {
    def apply(x : BigInt) : Vector[BigInt] = () match {
        case _ if (x == 1) => Vector()
        case _ if x isProbablePrime 30 => Vector(x)
        case _ => {
            val d = someDivisor(x)
            apply(d) ++ apply(x / d)
        }
    }

    def someDivisor(N : BigInt) : BigInt = () match {
        case _ if N % 2 == 0 => 2
        case _ if N % 3 == 0 => 3
        case _ if N % 5 == 0 => 5
        case _ => {
            val c = BigInt(N.bitLength, util.Random)
            val r = BigInt(N.bitLength, util.Random)
            def f(x : BigInt) = (x * x + c) % N
            (Iterator iterate (0, 0, r, f(r))) {
                case (n1, n2, x1, x2) if n2 > n1 + n1 => (n2, n2 + 1, x2, f(x2))
                case (n1, n2, x1, x2) => (n1, n2 + 1, x1, f(x2))
            }.map{
                case (n1, n2, x1, x2) => (x1 - x2) gcd N
            }.find{_ != 1}.get match {
                case N => someDivisor(N)
                case x => x
            }
        }
    }
}

