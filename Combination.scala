object Combination {
    def choose(n : Int, k : Int) = (BigInt(1) /: (1 to k)) {
        (s, x) => s * (n + 1 - x) / x
    }
}
