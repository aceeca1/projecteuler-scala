object P074 {
    val factorial = (1 to 9 scanLeft 1){_ * _}.toArray
    def next(n : Int) = n.toString.map{x => factorial(x.asDigit)}.sum

    val n = 1000000
    val chainT = new Array[Int](factorial(9) * 7)

    def chain(n : Int, m : Int) : (Boolean, Int) = chainT(n) match {
        case 0 => {
            chainT(n) = m
            val c = chain(next(n), m - 1)
            val b = c._1 && chainT(n) != 0
            val x = if (c._1) c._2 else c._2 + 1
            chainT(n) = x
            (b, x)
        }
        case t if t < 0 => {
            chainT(n) = 0
            (true, t - m)
        }
        case t => (false, t)
    }

    def main(args : Array[String]) {
        println(1 to 1000000 count {chain(_, -1)._2 == 60})
    }
}
