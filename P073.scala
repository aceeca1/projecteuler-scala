object P073 {
    def phiTo(n : Int, p : Vector[Int]) : Int = p.size match {
        case 0 => n
        case _ => phiTo(n, p.tail) - phiTo(n / p.head, p.tail)
    }

    def phiThirdToHalf(n : Int) = {
        val p = Prime.factorize(n).distinct
        phiTo((n - 1) / 2, p) - phiTo(n / 3, p)
    }

    def main(args : Array[String]) {
        println((1 to 12000 map phiThirdToHalf).sum)
    }
}
