object P048 {
    def main(args : Array[String]) {
        val x = (1 to 1000).map{x => BigInt(x) pow x}.sum
        println(x.toString takeRight 10)
    }
}
