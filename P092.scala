object P092 {
    val n = 10000000
    val endToT = Array.fill(n)(-1); {
        endToT(1) = 1
        endToT(89) = 89
    }
    def endTo(k : Int) : Int = {
        if (k < n && endToT(k) != -1) return endToT(k)
        val s = for {
            ki <- k.toString
            d = ki.asDigit
        } yield d * d
        val result = endTo(s.sum)
        if (k < n) endToT(k) = result
        result
    }

    def main(args : Array[String]) {
        println(1 until n count {endTo(_) == 89})
    }
}
