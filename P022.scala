object P022 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/names.txt").mkString
        val s = r.split(",").map{x => x.substring(1, x.size - 1)}.sorted
        val x = for (i <- s.indices) yield s(i).map{_ - 'A' + 1L}.sum * (i + 1)
        println(x.sum)
    }
}
