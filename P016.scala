object P016 {
    def main(args : Array[String]) {
        val x = BigInt(1) << 1000
        println(x.toString.map{_.asDigit}.sum)
    }
}
