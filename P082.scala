object P082 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/matrix.txt").getLines
        val s = r.map{_ split "," map {_.toInt}}.toBuffer
        val d = Array.fill(s.size, s.head.size)(Int.MaxValue)
        val q = new collection.mutable.Queue[(Int, Int)]
        for (i <- d.indices) {
            d(i)(0) = s(i)(0)
            q += Tuple2(i, 0)
        }
        def SPFA() : Int = {
            val (x1, x2) = q.dequeue()
            def relax(n1 : Int, n2 : Int) {
                if (!d.isDefinedAt(n1)) return
                if (!d(n1).isDefinedAt(n2)) return
                val v = d(x1)(x2) + s(n1)(n2)
                if (v < d(n1)(n2)) {
                    d(n1)(n2) = v
                    q += Tuple2(n1, n2)
                }
            }
            relax(x1, x2 + 1)
            relax(x1 + 1, x2)
            relax(x1 - 1, x2)
            if (q.isEmpty) d.map{_.last}.min else SPFA()
        }
        println(SPFA())
    }
}
