object P090 {
    def main(args : Array[String]) {
        val s = for (i <- (0 to 9).combinations(6)) yield {
            val x = () match {
                case _ if i contains 6 => i :+ 9
                case _ if i contains 9 => i :+ 6
                case _ => i
            }
            collection.immutable.BitSet(x : _*)
        }
        val sB = s.toBuffer
        val r = 1 to 9 map {x => (x * x / 10, x * x % 10)}
        val answer = for {
            i1 <- sB
            i2 <- sB
        } yield r forall {
            case (x1, x2) => i1(x1) && i2(x2) || i1(x2) && i2(x1)
        }
        println(answer.count(identity) / 2)
    }
}
