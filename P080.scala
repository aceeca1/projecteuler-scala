object P080 {
    def sqrt(x : BigInt) = {
        def newton(s0 : BigInt) : BigInt = {
            val s1 = (s0 + x / s0 + 1) / 2
            if (s0 == s1) s0 else newton(s1)
        }
        val s = newton(BigInt(1) << (x.bitLength / 2))
        if (s * s > x) s - 1 else s
    }

    def main(args : Array[String]) {
        val gg2 = BigInt(10) pow 200
        val answer = for {
            i <- 1 to 100
            xi = gg2 * i
            si = sqrt(xi)
            if (si * si < xi)
        } yield si.toString.take(100).map{_.asDigit}.sum
        println(answer.sum)
    }
}
