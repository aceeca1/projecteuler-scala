object P038 {
    def main(args : Array[String]) {
        val answer = for {
            xi <- 1 to 9999
            ni <- 2 to 9
            s = (1 to ni).map{xi * _}.mkString
            if (s.sorted == "123456789")
        } yield s
        println(answer.max)
    }
}
