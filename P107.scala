object P107 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/network.txt").getLines
        val e = r.map{
            _ split "," map {
                _ match {
                    case "-" => Int.MaxValue
                    case x => x.toInt
                }
            }
        }.toArray
        val answer = {
            val d = e(0).clone
            d(0) = 0
            def Prim(cost : Int) : Int = {
                val ns = 0 until d.size filter {d(_) > 0}
                if (ns.isEmpty) return cost
                val n = ns minBy d
                val nCost = d(n)
                d(n) = 0
                for (i <- 0 until d.size) d(i) = d(i) min e(n)(i)
                return Prim(cost + nCost)
            }
            Prim(0)
        }
        val origin = e.flatten.filter{_ != Int.MaxValue}.sum / 2
        println(origin - answer)
    }
}
