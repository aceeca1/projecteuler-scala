object P060 {
    def isPrime(x : String) = if (x.size <= 7) Prime(x.toInt) else {
        BigInt(x) isProbablePrime 30
    }

    case class Node(v : Vector[Int]) {
        val prio = -(v.sum + v.last * (5 - v.size))
        val valid = {
            val last = v.last.toString
            v.init forall {x => (v.last + x) % 3 != 0 && isPrime(x + last) && isPrime(last + x)}
        }
        def childs = {
            val next = Prime.next(v.last)
            val child = if (valid) Vector(Node(v :+ next)) else Vector()
            val sibling = Node(v.init :+ next)
            child :+ sibling
        }
        def succeed = valid && v.size == 5
    }

    val q = collection.mutable.PriorityQueue(Node(Vector(2)))(Ordering.by{_.prio})

    def search() : Node = {
        val x = q.dequeue
        if (x.succeed) x else {
            q.enqueue(x.childs : _*)
            search()
        }
    }

    def main(args : Array[String]) {
        println(search().v.sum)
    }
}
