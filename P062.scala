object P062 {
    def main(args : Array[String]) {
        val x3 = Iterator.from(1).map{x => (x.toLong * x * x).toString}
        val answer = for {
            i1 <- (Infinite group x3){_.size}
            x = i1.groupBy{_.toString.sorted}.values
            i2 <- x.filter{_.size == 5}.map{_.head}.toBuffer.sorted
        } yield i2
        println(answer.next)
    }
}
