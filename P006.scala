object P006 {
    def sqr(x : Int) = x * x
    def main(args : Array[String]) {
        val sqrsum = sqr((1 to 100).sum)
        val sumsqr = ((1 to 100) map sqr).sum
        println(sqrsum - sumsqr)
    }
}
