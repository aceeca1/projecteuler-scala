object P053 {
    def main(args : Array[String]) {
        val answer = (1 to 100).scanLeft(IndexedSeq(1.0)){
            (s, x) => 0 to s.size map {
                i => (s.lift(i - 1) ++ s.lift(i)).sum
            }
        }.flatten count {_ > 1e6}
        println(answer)
    }
}
