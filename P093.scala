object P093 {
    def mayGet(x : Vector[Double]) : Iterator[Double] = x.size match {
        case 1 => x.toIterator
        case _ => {
            for {
                xi @ Vector(x1, x2) <- x combinations 2
                x3 <- Array(
                    x1 + x2, x1 - x2, x2 - x1,
                    x1 * x2, x1 / x2, x2 / x1)
                r <- mayGet((x diff xi) :+ x3)
            } yield r
        }
    }

    def longestFrom1(x : Iterator[Double]) = {
        val xI = for {
            xi <- x
            n = xi.toInt
            if (n >= 1 && n == xi)
        } yield n
        val xS = collection.immutable.BitSet(xI.toBuffer : _*)
        Iterator from 1 indexWhere {!xS(_)}
    }

    def main(args : Array[String]) {
        val answer = Vector.iterate(1.0, 9){_ + 1.0} combinations 4 maxBy {
            x => longestFrom1(mayGet(x))
        }
        println(answer.map{_.toInt}.mkString)
    }
}
