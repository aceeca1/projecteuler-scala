object P084 {
    val A = {
        val ThreeDoubleJail = 3.0 / (1 << 10)
        val r = for {
            i1 <- 1 to 4
            i2 <- 1 to 4
        } yield i1 + i2
        val d = r groupBy identity mapValues {
            x => {
                val v = x.size.toDouble / 16
                if (x.head % 2 == 0) v - ThreeDoubleJail else v
            }
        }
        val a = Array.tabulate(40, 40) {
            (n1, n2) => {
                val x = n2 - n1 match {
                    case x if x < 0 => x + 40
                    case x => x
                }
                val v = d.getOrElse(x, 0.0)
                if (n2 == 10) v + ThreeDoubleJail * 4.0 else v
            }
        }
        for {
            (k, v) <- Array(
                30 -> Array.fill(16)(10),
                7  -> Array(0, 10, 11, 24, 39, 5, 15, 15, 12, 4),
                22 -> Array(0, 10, 11, 24, 39, 5, 25, 25, 28, 19),
                36 -> Array(0, 10, 11, 24, 39, 5, 5,  5,  12, 33),
                2  -> Array(0, 10),
                17 -> Array(0, 10),
                33 -> Array(0, 10))
            ai <- a
        } {
            val share = ai(k) / 16.0
            ai(k) = share * (16.0 - v.size)
            for (vi <- v) ai(vi) += share
        }
        a
    }

    def matrixMultiply(x1 : Array[Array[Double]], x2 : Array[Array[Double]]) = {
        for (i1 <- x1) yield
            for (i2 <- x2.transpose) yield
                (i1, i2).zipped.map{_ * _}.sum
    }

    def main(args : Array[String]) {
        val aSqr = Iterator.iterate(A){x => matrixMultiply(x, x)}
        val pInf = aSqr.drop(30).next.head
        val answer = pInf.indices sortBy {-pInf(_)} take 3
        println(answer.mkString)
    }
}
