object P078 {
    def p(n : Int) = {
        val r = new Array[Int](n)
        r(0) = 1
        for (p <- 1 until n) {
            def traverse(q : Int) {
                r(q + p) = r(q + p) + r(q) match {
                    case x if x >= 1000000 => x - 1000000
                    case x => x
                }
                if (q + 1 + p < n) traverse(q + 1)
            }
            traverse(0)
        }
        r
    }

    def main(args : Array[String]) {
        val answer = (Iterator iterate (0, p(1))) {
            case (x, a) if x + 1 >= a.size => (x + 1, p(a.size.*(1.5).toInt + 1))
            case (x, a) => (x + 1, a)
        } find {
            case (x, a) => a(x) == 0
        }
        println(answer.get._1)
    }
}
