object P106 {
    def main(args : Array[String]) {
        val n = 12
        val answer = for {
            i <- 0 to n / 2 - 2
            x1 = Combination.choose(n, i * 2 + 4)
            x2 = Combination.choose(i * 2 + 3, i)
        } yield x1 * x2
        println(answer.sum)
    }
}
