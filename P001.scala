object P001 {
    def main (args : Array[String]) {
        val m35 = for {
            i <- 1 until 1000
            if i % 3 == 0 || i % 5 == 0
        } yield i
        println(m35.sum)
    }
}
