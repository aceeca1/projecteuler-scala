object P004 {
    def main(args : Array[String]) {
        val p = for {
            i1 <- 100 until 1000
            i2 <- 100 until 1000
            x = i1 * i2
            xStr = x.toString
            if (xStr == xStr.reverse)
        } yield x
        println(p.max)
    }
}
