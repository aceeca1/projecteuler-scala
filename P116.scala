object P116 {
    def main(args : Array[String]) {
        val red = Iterator.iterate((1L, 2L)) {
            case (t0, t1) => (t1, t1 + t0)
        }.drop(50 - 1).next._1 - 1L
        val green = Iterator.iterate((1L, 1L, 2L)) {
            case (t0, t1, t2) => (t1, t2, t2 + t0)
        }.drop(50 - 1).next._1 - 1L
        val blue = Iterator.iterate((1L, 1L, 1L, 2L)) {
            case (t0, t1, t2, t3) => (t1, t2, t3, t3 + t0)
        }.drop(50 - 1).next._1 - 1L
        println(red + green + blue)
    }
}
