object P052 {
    def main(args : Array[String]) {
        val answer = for {
            i <- Iterator from 1
            s2 = (i * 2).toString.sorted
            if (s2 == (i * 6).toString.sorted)
            if (s2 == (i * 5).toString.sorted)
            if (s2 == (i * 4).toString.sorted)
            if (s2 == (i * 3).toString.sorted)
        } yield i
        println(answer.next)
    }
}
