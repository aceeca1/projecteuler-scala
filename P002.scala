object P002 {
    def fibFrom(x1 : Int, x2 : Int) : Stream[Int] = x1 #:: fibFrom(x2, x1 + x2)
    val fib = fibFrom(1, 1)
    def main(args : Array[String]) {
        val x = fib takeWhile {_ <= 4000000} filter {_ % 2 == 0}
        println(x.sum)
    }
}
