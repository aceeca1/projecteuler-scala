object P113 {
    def main(args : Array[String]) {
        val n = 100
        val a1 = Combination.choose(n + 10, 10)
        val a2 = Combination.choose(n + 9, 9)
        println(a1 + a2 - 10 * n - 2)
    }
}
