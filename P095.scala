object P095 {
    def divisorSum(n : Int) = {
        val m = Prime factorize n groupBy identity
        m.values.map{
            x => (x scanLeft 1){_ * _}.sum
        }.product - n
    }

    val n = 1000000
    val ch = Array.fill(n + 1)(0); {
        def traverse(c : Int) {
            def compute(c : Int, m : Int) : Int = {
                if (c < 1 || n < c || ch(c) > 0) return 1
                if (ch(c) < 0) {
                    ch(c) = 0
                    return ch(c) - m
                }
                ch(c) = m
                val r = compute(divisorSum(c), m - 1)
                val b = ch(c) == 0
                ch(c) = r
                return if (b) 1 else r
            }
            compute(c, -1)
            if (c + 1 <= n) traverse(c + 1)
        }
        traverse(1)
    }

    def main(args : Array[String]) {
        println(1 to 1000000 maxBy ch)
    }
}
