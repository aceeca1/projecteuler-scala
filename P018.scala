object P018 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/problem=18").mkString
        val s = "[0-9][0-9][0-9 ]*(?=<br|</p)".r.findAllMatchIn(r).map{
            _.matched split " " map {_.toInt}
        }.toBuffer
        val answer = (s.init :\ s.last.toIndexedSeq) {
            (x, f) => for (i <- x.indices) yield x(i) + (f(i) max f(i + 1))
        }
        println(answer.head)
    }
}
