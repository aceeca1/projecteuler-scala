object P028 {
    def main(args : Array[String]) {
        val s1 = Iterator.iterate(2){_ + 2} take (1001 / 2)
        val s2 = s1 flatMap {x => Iterator(x, x, x, x)}
        println(s2.scanLeft(1){_ + _}.sum)
    }
}
