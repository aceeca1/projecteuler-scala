object P104 {
    def main(args : Array[String]) {
        val m = 1000000000
        val answer = Iterator.iterate(1.0, 1.0, 1, 1){
            case (a1, a2, b1, b2) => {
                val a3 = a1 + a2
                val b3 = (b1 + b2) % m
                if (a3 > m) (a2 / m, a3 / m, b2, b3) else (a2, a3, b2, b3)
            }
        } indexWhere {
            case (_, a, _, b) => {
                val t = b.toString.sorted == "123456789"
                t && (a * m).formatted("%.0f").take(9).sorted == "123456789"
            }
        }
        println(answer + 2)
    }
}
