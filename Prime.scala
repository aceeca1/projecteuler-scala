object Prime {
    case class PrimeF(size : Int) {
        val p = Array.ofDim[Int](size); {
            def testing(n : Int, prev : Int) {
                def sieving(s : Int, t : Int) {
                    if (n * s >= size) return
                    p(n * s) = s
                    if (s < t) sieving(p(s), t)
                }
                if (n < size) p(n) match {
                    case 0 => {p(prev) = n; sieving(2, n); testing(n + 1, n)}
                    case x => {sieving(2, x); testing(n + 1, prev)}
                } else p(prev) = Int.MaxValue
            }
            testing(2, 0)
        }
    }

    var p = PrimeF(512).p

    private def get(n : Int) = {
        if (n >= p.size) this.synchronized {
            val newSize = n + 1 max p.size * 2
            if (newSize > p.size) p = PrimeF(newSize).p
        }
        p(n)
    }

    def apply(n : Int) = n >= 2 && get(n) >= n

    def minFactor(n : Int) = get(n) min n

    def incToPrime(n : Int) : Int = if (apply(n)) n else incToPrime(n + 1)

    def next(n : Int) : Int = get(n) match {
        case Int.MaxValue => {get(p.size); next(n)}
        case x => x
    }

    case class From(n : Int) extends collection.SeqView[Int, From] {
        def iterator = Iterator.iterate(incToPrime(n))(next)
        def apply(idx : Int) = drop(idx).head
        def length = Int.MaxValue
        def underlying = this
    }

    val all = From(2)

    def factorize(n : Int) : Vector[Int] = n match {
        case 1 => Vector()
        case _ => {val p = minFactor(n); p +: factorize(n / p)}
    }

    def phi(n : Int) : Array[Int] = {
        apply(n)
        val returnValue = new Array[Int](n + 1)
        returnValue(1) = 1
        def traverse(i : Int) {
            val p = minFactor(i)
            val q = i / p
            val pp = minFactor(q)
            returnValue(i) = returnValue(q) * (if (p == pp) p else p - 1)
            if (i < n) traverse(i + 1)
        }
        traverse(2)
        returnValue
    }
}
