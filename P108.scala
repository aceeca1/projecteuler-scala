object P108 {
    def value(x : Array[Int]) = {
        val p = Prime.all.toIterator
        x.map{Math.pow(p.next(), _)}.product
    }

    def sqrDivisorNum(x : Array[Int]) = {
        x.map{_ * 2 + 1.0}.product
    }

    def main(args : Array[String]) {
        val m = 1000 * 2 + 1
        var answer = Double.MaxValue
        def visit(x : Array[Int]) : Unit = sqrDivisorNum(x) match {
            case s if s >= m => answer = value(x)
            case _ => 1 to x.last map {x :+ _} takeWhile {
                n => value(n) < answer
            } foreach visit
        }
        for {
            i <- Iterator from 1 map {Array(_)} takeWhile {
                n => value(n) < answer
            }
        } visit(i)
        println(answer)
    }
}
