object P010 {
    def main(args : Array[String]) {
        println(Prime.all.takeWhile{_ <= 2000000}.map{_.toLong}.sum)
    }
}
