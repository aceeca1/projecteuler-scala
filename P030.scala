object P030 {
    def p5(x : Int) = x * x * x * x * x
    def main(args : Array[String]) {
        val answer = (10 to p5(9) * 6).filter{
            x => x == x.toString.map{si => p5(si.asDigit)}.sum
        }
        println(answer.sum)
    }
}
