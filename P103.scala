object P103 {
    val answer = {
        import collection.immutable.BitSet
        var opt = Int.MaxValue
        var optS : Array[Int] = null
        def search(s : Array[Int], t : BitSet) {
            val (lowerBound, upperBound) = s.length match {
                case 7 => {
                    if (s.sum >= opt) return
                    opt = s.sum
                    optS = s
                    return
                }
                case 6 => {
                    val p = s(3) + s(4) + s(5) - s(0) - s(1) - s(2)
                    (10 max p, s.head - 1)
                }
                case 5 => {
                    val p = ((s(2) + s(3) + s(4)) * 3 + 9) / 4 - s(0) - s(1)
                    (11 max p, s.head - 1)
                }
                case 4 => {
                    val p = (s(1) + s(2) + s(3)) / 2 + 2 - s(0)
                    (12 max p, s.head - 1)
                }
                case 3 => {
                    val p = (s(0) + s(1) + s(2) + 9) / 4
                    (13 max p, s.head - 1)
                }
                case 2 => {
                    val p = (opt - 1) / 2 - s(0) - s(1)
                    (14, s.head - 1 min p)
                }
                case 1 => {
                    val p = (3 * opt - 85) / 8 - s(0)
                    (15, s.head - 1 min p)
                }
            }
            for (i <- lowerBound to upperBound) {
                val sNext = i +: s
                val tNext = t + i | (t map {_ + i}) | (t map {x => (x - i).abs})
                if (!tNext(0)) search(sNext, tNext)
            }
        }
        for (i <- 16 to 57) search(Array(i), BitSet(i))
        optS
    }

    def main(args : Array[String]) {
        println(answer.mkString)
    }
}
