object P068 {
    def main(args : Array[String]) {
        val vB = Vector(9, 8, 7, 6, 5, 4, 3, 2, 1)
        val answer = for {
            a1 <- Iterator.iterate(6){_ - 1}
            b2 <- vB diff Vector(a1)
            b3 <- vB diff Vector(a1, b2)
            s = a1 + b2 + b3
            vA = Range(9, a1, -1) :+ 10
            a2 <- vA diff Vector(a1, b2, b3) filter {_ > a1}
            b4 <- vB diff Vector(a1, b2, b3, a2)
            if (a2 + b3 + b4 == s)
            a3 <- vA diff Vector(a1, b2, b3, a2, b4) filter {_ > a1}
            b5 <- vB diff Vector(a1, b2, b3, a2, b4, a3)
            if (a3 + b4 + b5 == s)
            a4 <- vA diff Vector(a1, b2, b3, a2, b4, a3, b5)
            b1 <- vB diff Vector(a1, b2, b3, a2, b4, a3, b5, a4)
            if (a4 + b5 + b1 == s)
            a5 <- vA diff Vector(a1, b2, b3, a2, b4, a3, b5, a4, b1)
            if (a5 + b1 + b2 == s)
        } yield Vector(a1, b2, b3, a2, b3, b4, a3, b4, b5, a4, b5, b1, a5, b1, b2)
        println(answer.next.mkString)
    }
}
