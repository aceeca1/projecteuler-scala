object P017 {
    def pronounce : Int => List[String] = {
        case 1 => List("one")
        case 2 => List("two")
        case 3 => List("three")
        case 4 => List("four")
        case 5 => List("five")
        case 6 => List("six")
        case 7 => List("seven")
        case 8 => List("eight")
        case 9 => List("nine")
        case 10 => List("ten")
        case 11 => List("eleven")
        case 12 => List("twelve")
        case 13 => List("thirteen")
        case 14 => List("fourteen")
        case 15 => List("fifteen")
        case 16 => List("sixteen")
        case 17 => List("seventeen")
        case 18 => List("eighteen")
        case 19 => List("nineteen")
        case 20 => List("twenty")
        case 30 => List("thirty")
        case 40 => List("forty")
        case 50 => List("fifty")
        case 60 => List("sixty")
        case 70 => List("seventy")
        case 80 => List("eighty")
        case 90 => List("ninety")
        case 1000 => List("one", "thousand")
        case x if x % 100 == 0 => pronounce(x / 100) :+ "hundred"
        case x if x < 100 => pronounce(x / 10 * 10) ++ pronounce(x % 10)
        case x if x < 1000 => {
            val h1 = pronounce(x / 100 * 100)
            val h2 = pronounce(x % 100)
            h1 ++ ("and" +: h2)
        }
    }

    def main(args : Array[String]) {
        println((1 to 1000).map{pronounce(_).map{_.size}.sum}.sum)
    }
}
