object P066 {
    def main(args : Array[String]) {
        val answer = 1 to 1000 maxBy {
            x => PellEquation(x).getOrElse(BigInt(0), null)._1
        }
        println(answer)
    }
}
