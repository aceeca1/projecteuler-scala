object P101 {
    def main(args : Array[String]) {
        val answer = (1 to 10).map{
            x => Iterator.iterate(1L){_ * -x}.take(11).sum
        }.inits.map{
            v => Iterator.iterate(v){
                v => (v.tail, v.init).zipped.map{_ - _}
            }.take(v.size).map{_.lastOption getOrElse 0L}.sum
        }.sum
        println(answer)
    }
}
