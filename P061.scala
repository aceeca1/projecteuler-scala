object P061 {
    val z = 3 to 8 map {n => 1000 to 9999 filter {PolygonalNum.test(n)(_)}}
    def main(args : Array[String]) {
        val answer = for {
            pi <- (0 to 4).permutations
            x5 <- z(5)
            x0 <- z(pi(0))
            if (x5 % 100 == x0 / 100)
            x1 <- z(pi(1))
            if (x0 % 100 == x1 / 100)
            x2 <- z(pi(2))
            if (x1 % 100 == x2 / 100)
            x3 <- z(pi(3))
            if (x2 % 100 == x3 / 100)
            x4 <- z(pi(4))
            if (x3 % 100 == x4 / 100)
            if (x4 % 100 == x5 / 100)
        } yield x0 + x1 + x2 + x3 + x4 + x5
        println(answer.next)
    }
}
