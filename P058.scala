object P058 {
    def isPrime(x : Int) = BigInt(x) isProbablePrime 30
    def main(args : Array[String]) {
        val s1 = Iterator.iterate(2){_ + 2}
        val s2 = s1 flatMap {x => Iterator(x, x, x, x)}
        val s3 = s2.scanLeft(1){_ + _}
        val s4 = s3.scanLeft(0){(s, x) => if (isPrime(x)) s + 1 else s}
        val s5 = s4.zipWithIndex drop 5 grouped 4 map {_.head}
        val x = s5 indexWhere {case (x, i) => x.toDouble / i < 0.1}
        println(x + x + 3)
    }
}
