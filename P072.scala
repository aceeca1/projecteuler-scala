object P072 {
    def main(args : Array[String]) {
        val phi = Prime.phi(1000000)
        println(phi.drop(2).map{_.toLong}.sum)
    }
}
