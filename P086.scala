object P086 {
    def isSquare(x : Int) = {
        val s = math.sqrt(x).toInt
        x == s * s
    }

    def withMaxLength(c : Int) = {
        val answer = for {
            p <- 2 to 2 * c
            if (isSquare(p * p + c * c))
            lowerBound = 1 max (p - c)
            upperBound = p / 2
        } yield (upperBound - lowerBound + 1)
        answer.sum
    }

    def main(args : Array[String]) {
        val s1 = Iterator from 1 map withMaxLength
        val s2 = (s1 scanLeft 0){_ + _}
        println(s2 indexWhere {_ > 1000000})
    }
}
