object P075 {
    def main(args : Array[String]) {
        val L = 1500000
        val x = for {
            n <- Iterator from 1 takeWhile {n => 2 * (1 + n) * (1 + n + n) <= L}
            s <- Iterator.iterate(n + 1){_ + 2} filter {
                BigInt(n).gcd(_) == 1
            } map {m => 2 * m * (m + n)} takeWhile {_ <= L}
            sk <- Iterator.iterate(s){_ + s} takeWhile {_ <= L}
        } yield sk
        val xb = x.toBuffer
        println(xb groupBy identity count {_._2.size == 1})
    }
}
