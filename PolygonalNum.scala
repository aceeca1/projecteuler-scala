object PolygonalNum {
    def apply(s : Long)(n : Long) = n * (n - 1) / 2 * (s - 2) + n

    def test(s : Long)(x : Long) = s match {
        case 3L => x == apply(3L)(math.sqrt(x * 2.0).toLong)
        case 4L => x == apply(4L)(math.sqrt(x).toLong)
        case _  => x == apply(s)(math.sqrt(x * 2.0 / (s - 2)).toLong + 1)
    }
}
