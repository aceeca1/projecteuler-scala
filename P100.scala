object P100 {
    def main(args : Array[String]) {
        val c1 = 3 + 2 * math.sqrt(2)
        val c2 = 2 - math.sqrt(2)
        val c3 = math.sqrt(2) - 1
        val answer = Iterator.iterate(1.0){_ * c1}.map{
            x => ((c2 * x / 8).floor + 1, (c3 * x / 4).floor + 1)
        }.find{_._2 > 1e12}.get._1.formatted("%.0f")
        println(answer)
    }
}
