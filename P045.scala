object P045 {
    def main(args : Array[String]) {
        val answer = Iterator.from(144).map{PolygonalNum(6)(_)}.find{
            n => PolygonalNum.test(5)(n) && PolygonalNum.test(3)(n)
        }
        println(answer.get)
    }
}
