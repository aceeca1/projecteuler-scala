object P105 {
    def special(s : IndexedSeq[Int]) = {
        val ss = s.sorted
        val h = (ss.length + 1) / 2
        ss.take(h).sum > ss.takeRight(h - 1).sum
    } && {
        import collection.immutable.BitSet
        (BitSet(s.head) /: s.tail) {
             (x, si) => x + si | x.map{_ + si} | x.map{n => (n - si).abs}
        }(0) == false
    }

    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/sets.txt").getLines
        val s = r map {_ split "," map {_.toInt}}
        val answer = for {
            i <- s
            if (special(i.toVector))
        } yield i.sum
        println(answer.sum)
    }
}
