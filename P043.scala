object P043 {
    def main(args : Array[String]) {
        val answer = for {
            i <- "0123456789".permutations
            if (i.substring(7, 10).toInt % 17 == 0)
            if (i.substring(6, 9).toInt % 13 == 0)
            if (i.substring(5, 8).toInt % 11 == 0)
            if (i.substring(4, 7).toInt % 7 == 0)
            if (i.substring(3, 6).toInt % 5 == 0)
            if (i.substring(2, 5).toInt % 3 == 0)
            if (i.substring(1, 4).toInt % 2 == 0)
            if (i.head != '0')
        } yield i.toLong
        println(answer.sum)
    }
}
