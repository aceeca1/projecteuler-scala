object PellEquation {
    def apply(n : Int) : Option[(BigInt, BigInt)] = {
        val s = (math sqrt n).toInt
        if (s * s == n) return None
        def isSolution(x : (Int, Int, Int, BigInt, BigInt)) = {
            x._4 * x._4 - x._5 * x._5 * n == 1
        }
        val x0 = (0, 1, s, BigInt(s), BigInt(1))
        if (isSolution(x0)) return Some((x0._4, x0._5))
        val v1 = n - s * s
        val a1 = (s + s) / v1
        val x1 = (s, v1, a1, BigInt(s * a1 + 1), BigInt(a1))
        (Iterator iterate (x0, x1)) {
            case ((u0, v0, a0, p0, q0), x1 @ (u1, v1, a1, p1, q1)) => {
                val u2 = a1 * v1 - u1
                val v2 = (n - u2 * u2) / v1
                val a2 = (s + u2) / v2
                val p2 = a2 * p1 + p0
                val q2 = a2 * q1 + q0
                (x1, (u2, v2, a2, p2, q2))
            }
        }.map{_._2}.find(isSolution).map{x => (x._4, x._5)}
    }
}
