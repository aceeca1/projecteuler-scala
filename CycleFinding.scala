object CycleFinding {
    def apply[T](x : T)(f : T => T) = (Iterator iterate (0, 0, x, x)) {
        case (n1, n2, x1, x2) if n2 > n1 + n1 => (n2, n2 + 1, x2, f(x2))
        case (n1, n2, x1, x2) => (n1, n2 + 1, x1, f(x2))
    }.drop(1).find{
        case (n1, n2, x1, x2) => x1 == x2
    }.get match {
        case (n1, n2, x1, x2) => n2 - n1
    }

    def startAndLength[T](x : T)(f : T => T) = {
        val length = apply(x)(f)
        def i = Iterator.iterate(x)(f)
        val start = i drop length zip i indexWhere {
            case (x1, x2) => x1 == x2
        }
        (start, length)
    }
}
