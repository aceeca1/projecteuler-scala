object P008 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/problem=8").mkString
        val s = "[0-9]+(?=<br)".r.findAllMatchIn(r).mkString.map{_.asDigit}
        println(s.sliding(5).map{_ reduce {_ * _}}.max)
    }
}
