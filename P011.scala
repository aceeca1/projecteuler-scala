object P011 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/problem=11").mkString
        val s = "[0-9].*(?=<br)".r.findAllMatchIn(r).map{
            _.matched.split("(<.*?>| )+").map{_.toInt}
        }.toBuffer
        val p = for {
            hi <- s.indices
            wi <- s.head.indices
            (dhi, dwi) <- Array((0, 1), (1, 1), (1, 0), (1, -1))
            if (s isDefinedAt hi + dhi * 3)
            if (s.head isDefinedAt wi + dwi * 3)
        } yield Iterator.iterate (hi, wi) {
            case (hi, wi) => (hi + dhi, wi + dwi)
        }.take(4).map{
            case (hi, wi) => s(hi)(wi)
        }.product
        println(p.max)
    }
}
