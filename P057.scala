object P057 {
    def main(args : Array[String]) {
        val answer = (Iterator iterate (BigInt(3), BigInt(2))) {
            case (x1, x2) => (x1 + x2 + x2, x1 + x2)
        } take 1000 count {
            case (x1, x2) => x1.toString.size > x2.toString.size
        }
        println(answer)
    }
}
