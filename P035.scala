object P035 {
    def shift(x : String, n : Int) = x.splitAt(n % x.size) match {
        case (x1, x2) => x2 + x1
    }

    def main(args : Array[String]) {
        val x = Prime.all takeWhile {_ < 1000000}
        val x0 = x map {_.toString}
        val x1 = x0 map {shift(_, 1)} intersect x0
        val x2 = x1 map {shift(_, 2)} intersect x1
        val x3 = x2 map {shift(_, 4)} intersect x2
        println(x3.size)
    }
}
