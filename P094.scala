object P094 {
    def main(args : Array[String]) {
        val n = 1000000000
        val c = 7 + 4 * math.sqrt(3)
        val c1 = c
        val x1 = Iterator.iterate(c1){_ * c} map {_.round + 2}
        val s1 = x1.takeWhile{_ < n}.sum
        val c2 = (2 - math.sqrt(3)) * c * c
        val x2 = Iterator.iterate(c2){_ * c} map {_.round - 2}
        val s2 = x2.takeWhile{_ < n}.sum
        println(s1 + s2)
    }
}
