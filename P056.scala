object P056 {
    def main(args : Array[String]) {
        val answer = for {
            a <- BigInt(1) to 100
            b <- 1 to 100
        } yield (a pow b).toString.map{_.asDigit}.sum
        println(answer.max)
    }
}
