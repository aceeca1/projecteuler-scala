object P032 {
    def main(args : Array[String]) {
        val s234 = for {
            i1 <- 10 to 99
            i2 <- 100 to 999
            x = i1 * i2
            if (x < 10000 && Array(i1, i2, x).mkString.sorted == "123456789")
        } yield x
        val s144 = for {
            i1 <- 1 to 9
            i2 <- 1000 to 9999
            x = i1 * i2
            if (x < 10000 && Array(i1, i2, x).mkString.sorted == "123456789")
        } yield x
        println((s234 ++ s144).distinct.sum)
    }
}
