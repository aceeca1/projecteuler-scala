object Palindrome {
    val incRe = "(.*)([^9])(9*)".r

    def inc(x : String) = x match {
        case incRe(x1, x2, x3) => x1 + (x2.head + 1).toChar + "0" * x3.size
        case _ => "1" + "0" * x.size
    }

    def next(x : String) = {
        val s1 = (x.size + 1) / 2
        val s2 = x.size - s1
        val h0 = x take s1
        def palin(h : String) = h + h.take(s2).reverse
        palin(h0) match {
            case p if (p > x) => p
            case _ => palin(inc(h0))
        }
    }

    def from(x : String) = Iterator.iterate(x)(next)
}
