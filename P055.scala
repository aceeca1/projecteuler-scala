object P055 {
    def lychrel(m : Int, x : String) : Boolean = {
        val n = (BigInt(x) + BigInt(x.reverse)).toString
        m match {
            case 0 => true
            case _ if n == n.reverse => false
            case _ => lychrel(m - 1, n)
        }
    }

    def main(args : Array[String]) {
        println(1 until 10000 count {x => lychrel(50, x.toString)})
    }
}
