object P012 {
    def main(args : Array[String]) {
        val tri = Iterator from 1 map {PolygonalNum(3)(_)}
        println(tri.find{Divisor.num(_) > 500}.get)
    }
}
