object P034 {
    val factorial = 0 to 9 map {2.to(_).product}

    def main(args : Array[String]) {
        val answer = 10 to factorial(9) * 7 filter {
            x => x == x.toString.map{d => factorial(d.asDigit)}.sum
        }
        println(answer.sum)
    }
}
