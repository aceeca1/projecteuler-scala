object P019 {
    def main(args : Array[String]) {
        val x = for {
            yi <- 1901 to 2000
            mi <- 1 to 12
            if {
                import java.util.Calendar._
                val x = getInstance
                x.set(yi, mi, 1)
                x.get(DAY_OF_WEEK) == SUNDAY
            }
        } yield null
        println(x.size)
    }
}
