object P037 {
    def isPrime(x : String) = BigInt(x) isProbablePrime 30

    def sumWithPrefix(x : String) : BigInt = {
        val childs = for {
            i <- "123579"
            xi = x + i
            if (isPrime(xi))
        } yield sumWithPrefix(xi)
        val cond = 1 until x.size forall {i => isPrime(x drop i)}
        if (x.size > 1 && cond) childs.sum + BigInt(x) else childs.sum
    }

    def main(args : Array[String]) {
        println(sumWithPrefix(""))
    }
}
