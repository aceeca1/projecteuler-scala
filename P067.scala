object P067 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/triangle.txt").getLines
        val s = r.map{_ split " " map {_.toInt}}.toBuffer
        val answer = (s.init :\ s.last.toIndexedSeq) {
            (x, f) => for (i <- x.indices) yield x(i) + (f(i) max f(i + 1))
        }
        println(answer.head)
    }
}
