object P036 {
    def main(args : Array[String]) {
        val answer = for {
            i <- Palindrome from "1" takeWhile {_.size < 7}
            n = i.toInt
            b = n.toBinaryString
            if (b == b.reverse)
        } yield n
        println(answer.sum)
    }
}
