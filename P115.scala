object P115 {
    def main(args : Array[String]) {
        val t = Vector.fill(50 - 1)(1L) :+ 2L
        val answer = Iterator.iterate((0L, t)) {
            case (s, t) => (s + t.head, t.tail :+ t.last + s + 2L)
        } indexWhere {
            case (s, t) => t.head > 1000000L
        }
        println(answer + 1)
    }
}
