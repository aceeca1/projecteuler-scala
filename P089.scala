object P089 {
    def main(args : Array[String]) {
        val r = io.Source.fromURL("http://projecteuler.net/project/roman.txt").getLines
        val answer = for (ri <- r) yield {
            ri.size - (ri /: Array(
                "IIII" -> "IV", "VV" -> "X", "VIV" -> "IX", "XXXXX" -> "L",
                "XXXX" -> "XL", "LL" -> "C", "LXL" -> "IC", "CCCCC" -> "D",
                "CCCC" -> "CD", "DD" -> "M", "DCD" -> "IM")) {
                (s, x) => s.replace(x._1, x._2)
            }.size
        }
        println(answer.sum)
    }
}
