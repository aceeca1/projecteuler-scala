object P050 {
    def main(args : Array[String]) {
        val n = 1000000
        val p = Prime.all takeWhile {_ < n}
        val pS = p.toSet
        val answer = p.tails.flatMap{
            _.scanLeft(0){_ + _}.drop(2).takeWhile{_ < n}.zipWithIndex.filter{
                case (x, i) => pS(x)
            }.lastOption
        }.maxBy{_._2}._1
        println(answer)
    }
}
