object Divisor {
    def num(x : BigInt) = {
        val m = Factorize(x) groupBy identity
        m.values.map{x => BigInt(x.size + 1)}.product
    }

    def sum(x : BigInt) = {
        val m = Factorize(x) groupBy identity
        m.values.map{
            x => (x scanLeft BigInt(1)){_ * _}.sum
        }.product - x
    }

    def apply(x : BigInt) = {
        val m = Factorize(x) groupBy identity
        (Vector(BigInt(1)) /: m.values) {
            (s, x) => s flatMap {si => (x scanLeft si){_ * _}}
        }
    }
}

