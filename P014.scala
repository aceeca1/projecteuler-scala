object P014 {
    val chainT = collection.mutable.HashMap((1L, 1))
    def f(x : Long) = if (x % 2 == 0) x / 2 else 3 * x + 1
    def chain(x : Long) : Int = chainT.getOrElseUpdate(x, chain(f(x)) + 1)

    def main(args : Array[String]) {
        println(1L until 1000000 maxBy chain)
    }
}
