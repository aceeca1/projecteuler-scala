object P039 {
    def main(args : Array[String]) {
        val p = for {
            k <- 1 to 500
            m <- Iterator from 1 takeWhile {x => k * x * x < 500}
            n <- 1 until m
            if (k * m * (m + n) <= 500)
            m2 = m * m
            n2 = n * n
            a = k * (m2 - n2)
            b = 2 * k * m * n
            c = k * (m2 + n2)
        } yield Vector(a, b, c).sorted
        println(p.distinct.groupBy{_.sum}.maxBy{_._2.size}._1)
    }
}
