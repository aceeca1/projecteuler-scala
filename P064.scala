object P064 {
    def oddCycle(n : Int) : Boolean = {
        val sqrtN = math sqrt n
        val sqrtNI = sqrtN.toInt
        if (sqrtNI * sqrtNI == n) return false
        CycleFinding((0, 0, n)){
            case (_, x1, x2) => {
                val fx2 = (n - x1 * x1) / x2
                val fx0 = ((sqrtN - x1) / fx2).toInt
                val fx1 = - fx0 * fx2 - x1
                (fx0, fx1, fx2)
            }
        } % 2 == 1
    }

    def main(args : Array[String]) {
        println(1 to 10000 count oddCycle)
    }
}
