object Infinite {
    type TO[T] = TraversableOnce[T]

    def flatten[T](x : TO[TO[T]])(implicit cmp : Ordering[T]) = new Iterator[T] {
        val p = x.toIterator.map{_.toIterator.buffered}.buffered
        val order = Ordering.by[BufferedIterator[T], T]{_.head}{cmp.reverse}
        val q = collection.mutable.PriorityQueue(p.head)(order)
        def hasNext = true
        def next() = {
            val qi = q.dequeue()
            val returnValue = qi.next()
            q.enqueue(qi)
            if (qi == p.head) {
                p.next()
                q.enqueue(p.head)
            }
            returnValue
        }
    }

    def diff[T](x1 : TO[T], x2 : TO[T])(implicit cmp : Ordering[T]) = new Iterator[T] {
        val i1 = x1.toIterator.buffered
        val i2 = x2.toIterator.buffered
        def hasNext = true
        def next() = cmp.compare(i1.head, i2.head) match {
            case -1 => {i1.next()}
            case  0 => {i1.next(); i2.next(); next()}
            case  1 => {i2.next(); next()}
        }
    }

    def cyclic[T](x : T*) = new Iterator[T] {
        var i = 0
        def hasNext = true
        def next() = {
            val returnValue = x(i)
            i = if (i == x.size - 1) 0 else i + 1
            returnValue
        }
    }

    def group[T, A](x : TO[T])(f : T => A) = new Iterator[Vector[T]] {
        val i = x.toIterator.buffered
        def hasNext = true
        def next() = {
            val c = i.next()
            val fc = f(c)
            val v = Vector.newBuilder[T]
            v += c
            while (f(i.head) == fc) v += i.next()
            v.result()
        }
    }
}
