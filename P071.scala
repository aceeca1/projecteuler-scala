object P071 {
    def main(args : Array[String]) {
        println(1 to 1000000 * 3 / 7 maxBy {x => x.toDouble / (x * 7 / 3 + 1)})
    }
}
