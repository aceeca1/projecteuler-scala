object P063 {
    def main(args : Array[String]) {
        def f(n : Int) = {
            val x = Iterator.iterate(BigInt(1)){_ * n}.zipWithIndex
            x.takeWhile{case (xi, i) => xi.toString.size >= i}.map{_._1}
        }
        val answer = 1 to 9 flatMap f
        println(answer.distinct.size)
    }
}
