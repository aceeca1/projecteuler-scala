object P031 {
    def main(args : Array[String]) {
        val n = 200
        val coins = Array(1, 2, 5, 10, 20, 50, 100, 200)
        val v = Array.fill(n + 1){0}
        v(0) = 1
        for {
            c <- coins
            i <- c to n
        } v(i) = v(i) + v(i - c)
        println(v(n))
    }
}
